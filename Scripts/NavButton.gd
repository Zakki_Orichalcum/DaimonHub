extends TextureButton


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var panel = "home"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _pressed():
	get_node("/root/Global").emit_signal("changePanel", panel)
