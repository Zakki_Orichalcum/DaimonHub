extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("/root/Global").connect("changePanel", self, "ChangeMainPanel")
	ChangeMainPanel("home")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func ChangeMainPanel(panel):
	match(panel):
		"recipe":
			LoadInMainPanel("res://Components/Recipe Screen.tscn")
		"news":
			LoadInMainPanel("res://Components/News Screen.tscn")
		"tasklist":
			LoadInMainPanel("res://Components/Tasklist Screen.tscn")
		"home":
			LoadInMainPanel("res://Components/Home Screen.tscn")
		_:
			LoadInMainPanel("res://Components/Home Screen.tscn")

func LoadInMainPanel(componentName):
	var component = load(componentName)
	var inst = component.instance()
	var node = get_node("Background/MainPanel")
	for child in node.get_children():
		node.remove_child(child)
	node.add_child(inst)
